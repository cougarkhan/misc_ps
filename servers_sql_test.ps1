function load_assemblies {
	param([string]$name)
	$startTime = get-date
	
	Write-Host "Loading .NET SQL Assembly " -nonewline
	Write-Host "$name" -nonewline -foregroundcolor "CYAN"
	Write-host " ... " -nonewline
	if ([System.Reflection.Assembly]::LoadWithPartialName($name)) {
		$endTime = get-date
		Write-Host "Completed." -foregroundcolor "GREEN" -nonewline
		$compTime = ($endTime - $startTime).TotalSeconds
		Write-Host " ($compTime secs)"
	} else {
		$endTime = get-date
		Write-Host "Failed" -foregroundcolor "RED" -nonewline
		$compTime = ($endTime - $startTime).TotalSeconds
		Write-Host " ($compTime secs)"
	}
}

function populate_sql {
	param ($servers)

	$sql_conn = new-object System.Data.SqlClient.SqlConnection
	$sql_conn.ConnectionString = "Data Source=LOCALHOST\SQLEXPRESS;Database=bis_servers;trusted_connection=true"
	$sql_conn.Open()

	$sql_command = new-object System.Data.SQLClient.SQLCommand
	$sql_command.Connection = $sql_conn

	foreach ($server in $servers) {
		$name = $server.name
		$db_name = $server.database
		$p_func = $server.primaryfunction
		$s_func = $server.secondaryfunction
		$etype = $server.environmenttype
		$instance = $server.instance
		$sql_command.CommandText = "INSERT INTO [Server_Names] VALUES ('$name','$etype')"
		$sql_command.ExecuteNonQuery() | out-null
		if ($db_name -ne "") {
			$sql_command.CommandText = "INSERT INTO [Server_DB] VALUES ('$db_name',(select ID from [Server_Names] Where [Server_Names].S_Name='$name'), 'Unknown', '$instance');"
			$sql_command.ExecuteNonQuery() | out-null
		}
		if ($p_func -ne "") {
			$sql_command.CommandText = "INSERT INTO [Server_Func] VALUES ('$p_func',(select ID from [Server_Names] Where [Server_Names].S_Name='$name'));"
			$sql_command.ExecuteNonQuery() | out-null
		}
		if ($s_func -ne "") {
			$sql_command.CommandText = "INSERT INTO [Server_Func] VALUES ('$s_func',(select ID from [Server_Names] Where [Server_Names].S_Name='$name'));"
			$sql_command.ExecuteNonQuery() | out-null
		}
	}

	$sql_conn.Close()

}
function read_sql {
	
	$sql_command = "SELECT 
					[Server_Names].S_Name,
					[Server_Names].Etype,
					[Server_Func].F_Name,
					[Server_DB].D_Name,
					[Server_DB].Version,
					[Server_DB].Instance
					FROM 
					[Server_Names] 
					INNER JOIN [Server_DB] ON [Server_Names].ID=[Server_DB].S_ID
					INNER JOIN [Server_Func] ON [Server_Names].ID=[Server_Func].S_ID"
	Invoke-Sqlcmd -ServerInstance localhost\sqlexpress -Database bis_servers -Query $sql_command | ft

}

load_assemblies "Microsoft.SqlServer.SMO"
load_assemblies "Microsoft.SqlServer.SmoExtended"
load_assemblies "Microsoft.SqlServer.ConnectionInfo"
load_assemblies "Microsoft.SqlServer.SqlEnum"
add-pssnapin sqlserverprovidersnapin100
add-pssnapin sqlservercmdletsnapin100

$servers = import-csv C:\temp\servers.csv

$objects = @()

foreach ($server in $servers) {
	$object = new-object System.Object
	
	foreach ($property in $server.PSObject.Properties) {
		$object | Add-Member $property.name $property.value
	}
	
	$objects += $object
}

populate_sql $objects

read_sql
