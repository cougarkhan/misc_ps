function check_port {
	param([string]$target,[int]$port)

	$socket = new-object Net.Sockets.TcpClient
	$socket.Connect($target,$port)
	$connected = $socket.Connected
	$socket.Close()
	$socket = $null
	
	if ($socket.Connected) {
		return $true
	} else {
		return $false
	}
}