function drop_db {
	param ([string]$server, [string]$database)
	
	$sqlServer = New-Object -TypeName  ("Microsoft.SqlServer.Management.Smo.Server") $server
	$db = $sqlServer.Databases[$database]
	
	$db.DatabaseOptions.UserAccess = "Single"
	
	Write-Host "Kill all open connections to " -nonewline
	Write-Host "$server" -nonewline -foregroundcolor "CYAN"
	Write-Host "..." -nonewline
	$sqlServer.KillAllProcesses($database)
	if ($sqlServer.EnumProcesses() | where { $_.Database -match $database}) {
		Write-Host "Failure." -foregroundcolor "RED"
	} else {
		Write-Host "Success." -foregroundcolor "GREEN"
	}
		
	Write-Host "Dropping Database " -nonewline
	Write-Host "$database" -nonewline -foregroundcolor "CYAN"
	Write-Host "..."  #-nonewline
	$db.drop()
}

function restore_db {
	param ($server, [string]$backupFile)
	
	$sqlServer = New-Object -TypeName  Microsoft.SqlServer.Management.Smo.Server $server
	$backupDevice = New-Object -TypeName Microsoft.SqlServer.Management.Smo.BackupDeviceItem ($backupFile, "File")
	$smoRestore = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Restore
	$smoRestoreFile = New-Object -TypeName Microsoft.SqlServer.Management.Smo.RelocateFile
	$smoRestoreLog = New-Object -TypeName Microsoft.SqlServer.Management.Smo.RelocateFile
	
	$smoRestore.NoRecovery = $false;
	$smoRestore.ReplaceDatabase = $true;
	$smoRestore.Action = "Database"
	$smoRestorePercentCompleteNotification = 10;
	$smoRestore.FileNumber = 1
	$smoRestore.Devices.Add($backupDevice)
	
	$smoRestoreDetails = $smoRestore.ReadBackupHeader($sqlServer)
	
	$databaseName = $smoRestoreDetails.Rows[0]["DatabaseName"]
	"Database Name from Backup Header : " + $databaseName
	 
	$smoRestore.Database = $databaseName
	 
	$smoRestoreFile.LogicalFileName = $databaseName + "_Data"
	$smoRestoreFile.PhysicalFileName = $sqlServer.Information.MasterDBPath + "\" + $smoRestore.Database + ".MDF"
	$smoRestoreLog.LogicalFileName = $databaseName + "_Log"
	$smoRestoreLog.PhysicalFileName = $sqlServer.Information.MasterDBLogPath + "\" + $smoRestore.Database + "_1.LDF"
	$smoRestore.RelocateFiles.Add($smoRestoreFile)
	$smoRestore.RelocateFiles.Add($smoRestoreLog)
	 
	drop_db $server $databaseName
	$smoRestore.SqlRestore($sqlServer)
}

function check_db_existence {
	param($instance,$database)
	
	if ($instance.Databases[$instance]) {
		Write-Host "$databaseName " -nonewline -foregroundcolor "CYAN"
		Write-Host "does not exist."
		return $true
	} else {
		Write-Host "$databaseName " -nonewline -foregroundcolor "CYAN"
		Write-Host "does exist."
		return $false
	}
}