import-module ActiveDirectory
set-alias tc test-connection

$ip_list = @{}
$ad_list = @()
$pc_list = gc "C:\temp\server_list_final"
$i = 1
$count = $pc_list.count
$debug = $false

foreach ($pc in $pc_list) {
	write-progress -activity "Gathering IP Addresses..." -status "Processing $pc.   $i/$count" -percentcomplete ($i / $count * 100)
	try {
		$ip = tc $pc -count 1 -ea stop | select -exp ipv4address
		$ip_list.add($pc,$ip)
	}
	catch {
		Write-host "$pc Offline" -foregroundcolor RED
		$ip_list.add($pc,"0.0.0.0")
	}
	try {
		$ad_pc = Get-AdComputer -Identity $pc -Properties networkaddress,comment,seealso
		if ($debug) {write-host "********* START DEBUG ************" }
		if ($debug) {$ad_pc }
		if ($debug) {$ip.IPAddressToString }
		if ($debug) {write-host "********* END DEBUG ************" }
		#$set_ip = [string]$ip.IPAddressToString
		#$ad_pc.networkAddress = $set_ip
		#Set-AdComputer -Instance $ad_pc
		gwmi win32_operatingsystem -computername $pc -ea stop | out-null
		$ad_list += $ad_pc
	}
	catch {
		Write-host "$pc not found in AD" -foregroundcolor YELLOW
		$ad_pc = Get-AdComputer -Identity $pc -Properties networkaddress,comment,seealso
		$ad_pc.seealso = "workgroup"
		set-adcomputer -instance $ad_pc
	}
	$i++
}

#$ip_list
$ad_list | select Name,networkAddress