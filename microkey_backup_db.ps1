#####################################################################################################################
#
# Script to backup MKMS_Default database
# Log files will be written to \\bops-file1\JobLogs
#
# Created on 2010-11-04
#
#####################################################################################################################

# Destination Backup Directory
$baseBackupDir = "D:\MKMS_Backup"
$folderList = get-childitem $baseBackupDir -recurse

# Format Date Strings
$seconds = (get-date).Second
if ($seconds.length -eq 1) { $seconds = "0$seconds" }

$minute = (get-date).Minute
if ($minute.length -eq 1) { $minute = "0$minute" }

$hour = (get-date).Hour
if ($hour.length -eq 1) { $hour = "0$hour" }

$day = (get-date).day.tostring()
if ($day.length -eq 1) { $day = "0$day" }

$month = (get-date).month.tostring()
if ($month.length -eq 1) { $month = "0$month" }

$year = (get-date).year.tostring()


$backupDate = "$year$month$day"
$fileAgeLimit = (get-date).AddDays(-14)
$today = (get-date).toshortdatestring()
$time = (get-date).toshorttimestring()

$logFileName = "$backupDate$hour-$minute-$seconds-bis-microkey-MKMS_Default_Backup"

$description = "File is older than 14 days and has been deleted."

# Create Log File
$logFile = "\\bops-file1\JobLogs\$logFileName.txt"

if (!(test-path $logFile)) {
    new-item $logFile -type file
}

# Delete files that are older than 14 days
if ($folderList) {
    foreach ($item in $folderList) {
        if ($item.CreationTime -lt $fileAgeLimit) {
            "{0,-9} {1,-12} {2,-30} {3,-28} {4,-20}" -f $today, $time, $item.FullName, $item.CreationTime.ToString(), $description | out-file $logFile -append
            if (test-path $item.directory) {
                Remove-Item -path $item.directory -recurse

            }
        }
    }
}

&'C:\Program Files\Sybase\SQL Anywhere 9\win32\dbbackup.exe' -c "uid=admin;pwd=admin;dsn=mkmsdefault" $baseBackupDir\mkms_backup_$backupDate -y |  out-file $logFile -append
exit