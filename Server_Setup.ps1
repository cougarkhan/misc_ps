#########################################################################################################
#
# Script to setup a server with certain variables
#
#
#########################################################################################################


#########################################################################################################
# Machine values
#########################################################################################################
#$machine = $(read-host "Please enter the Hostname or IP Address of the machine to be edited:")
$machine = "BIS-240"
$host_ComputerSystem = Get-WmiObject Win32_ComputerSystem -computername $machine
$host_NetworkAdapters = Get-WmiObject Win32_NetworkAdapterConfiguration -computername $machine | where { $_.IPEnabled -eq "TRUE" }
$host_LogicalDisks = Get-WmiObject Win32_LogicalDisk -computername $machine | where { $_.DriveType -eq 3 }

write-host "`n`n"
write-host "---------------" -foregroundcolor "YELLOW"
write-host "|  Hostname   |" -foregroundcolor "YELLOW"
write-host "---------------" -foregroundcolor "YELLOW"
write-host "`n"
write-host "Hostname: " $host_ComputerSystem.Name -foregroundcolor "RED"
write-host "Domain: " $host_ComputerSystem.Domain
write-host "Manufacturer: " $host_ComputerSystem.Manufacturer
write-host "`n"
write-host "---------------" -foregroundcolor "YELLOW"
write-host "|  Network    |" -foregroundcolor "YELLOW"
write-host "---------------" -foregroundcolor "YELLOW"
write-host "`n"
foreach ($adapter in $host_NetworkAdapters) {
    write-host $adapter.Description -foregroundcolor "GREEN"
    write-host "IP Address:" $adapter.IPAddress
    write-host "Gateway:" $adapter.DefaultIPGateway
    write-host "DNS Servers:" $adapter.DNSServerSearchOrder
    write-host "`n"
}
write-host "---------------" -foregroundcolor "YELLOW"
write-host "|  Disks      |" -foregroundcolor "YELLOW"
write-host "---------------" -foregroundcolor "YELLOW"
foreach ($disk in $host_LogicalDisks) {
    [float]$size = [Math]::Round($disk.Size / (1024 * 1024 * 1024),2)
    [float]$free = [Math]::Round($disk.FreeSpace / (1024 * 1024 * 1024),2)
    write-host "`nDevice ID: "$disk.DeviceID -foregroundcolor "GREEN"
    write-host "Description: "$disk.Description
    write-host "Disk Space: $free GB / $size GB"
}



write-host "`n`n"

function Configure_Server {
    $ntp_Server = "bops-dc1.bussops.ubc.ca"
    $timezone = "
















#$new_IP_Address = $(read-host "Please enter the new IP Address of the Primary Adapter:")
#$new_Hostname = $(read-host "Please enter the new Hostname of the machine:")

