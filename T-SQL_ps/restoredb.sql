USE [master]
GO
IF NOT DB_ID('AcsData') IS NULL
	BEGIN
		ALTER DATABASE [AcsData] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
		DROP DATABASE [AcsData]
		RESTORE DATABASE [AcsData] FROM  DISK = N'C:\DRS\Software\AcsData_db.bak' WITH  FILE = 1,  MOVE N'AcsData_Data' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AcsData.MDF',  MOVE N'AcsData_Log' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AcsData_1.LDF',  NOUNLOAD,  STATS = 10
	END
ELSE
	BEGIN
		RESTORE DATABASE [AcsData] FROM  DISK = N'C:\DRS\Software\AcsData_db.bak' WITH  FILE = 1,  MOVE N'AcsData_Data' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AcsData.MDF',  MOVE N'AcsData_Log' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AcsData_1.LDF',  NOUNLOAD,  STATS = 10
	END
IF NOT DB_ID('AcsLog') IS NULL
	BEGIN
		ALTER DATABASE [AcsLog] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
		DROP DATABASE [AcsLog]
		RESTORE DATABASE [AcsLog] FROM  DISK = N'C:\DRS\Software\AcsLog_db.bak' WITH  FILE = 1,  MOVE N'AcsLog_Data' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AcsLog.MDF',  MOVE N'AcsLog_Log' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AcsLog_1.LDF',  NOUNLOAD,  STATS = 10
	END
ELSE
	BEGIN
		RESTORE DATABASE [AcsLog] FROM  DISK = N'C:\DRS\Software\AcsLog_db.bak' WITH  FILE = 1,  MOVE N'AcsLog_Data' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AcsLog.MDF',  MOVE N'AcsLog_Log' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AcsLog_1.LDF',  NOUNLOAD,  STATS = 10
	END
IF NOT DB_ID('DSX-SystemSettings') IS NULL
	BEGIN
		ALTER DATABASE [DSX-SystemSettings] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
		DROP DATABASE [DSX-SystemSettings]
		RESTORE DATABASE [DSX-SystemSettings] FROM  DISK = N'C:\DRS\Software\DSX-SystemSettings_db.bak' WITH  FILE = 1,  MOVE N'DSX-SystemSettings_Data' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DSX-SystemSettings.MDF',  MOVE N'DSX-SystemSettings_Log' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DSX-SystemSettings_1.LDF',  NOUNLOAD,  STATS = 10
	END
ELSE
	BEGIN
		RESTORE DATABASE [DSX-SystemSettings] FROM  DISK = N'C:\DRS\Software\DSX-SystemSettings_db.bak' WITH  FILE = 1,  MOVE N'DSX-SystemSettings_Data' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DSX-SystemSettings.MDF',  MOVE N'DSX-SystemSettings_Log' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DSX-SystemSettings_1.LDF',  NOUNLOAD,  STATS = 10
	END
IF NOT DB_ID('DSX-Update') IS NULL
	BEGIN
		ALTER DATABASE [DSX-Update] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
		DROP DATABASE [DSX-Update]
		RESTORE DATABASE [DSX-Update] FROM  DISK = N'C:\DRS\Software\DSX-Update_db.bak' WITH  FILE = 1,  MOVE N'DSX-Update_Data' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DSX-Update.MDF',  MOVE N'DSX-Update_Log' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DSX-Update.LDF',  NOUNLOAD,  STATS = 10
	END
ELSE
	BEGIN
		RESTORE DATABASE [DSX-Update] FROM  DISK = N'C:\DRS\Software\DSX-Update_db.bak' WITH  FILE = 1,  MOVE N'DSX-Update_Data' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DSX-Update.MDF',  MOVE N'DSX-Update_Log' TO N'F:\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DSX-Update.LDF',  NOUNLOAD,  STATS = 10
	END
GO
USE [AcsData]
DROP SCHEMA [calvinlo]
DROP SCHEMA [dqiu]
DROP SCHEMA [acms]
DROP SCHEMA [acms-user]
DROP SCHEMA [DSX Update]
DROP SCHEMA [dsxWeb]
DROP SCHEMA [DSX User]
EXEC sp_droprolemember N'acms-user', N'acms'
DROP ROLE [acms-user]
DROP USER [aleghari]
DROP USER [calvinlo]
DROP USER [mpal]
DROP USER [dqiu]
DROP USER [acms]
DROP USER [DSX Update]
DROP USER [DSX User]
DROP USER [dsxWeb]
DROP USER [BUSSOPS\sadmin]
GO
USE [AcsLog]
DROP SCHEMA [calvinlo]
DROP SCHEMA [dqiu]
DROP SCHEMA [DSX User]
DROP USER [calvinlo]
DROP USER [mpal]
DROP USER [dqiu]
DROP USER [DSX User]
DROP USER [BUSSOPS\sadmin]
GO
USE [DSX-SystemSettings]
DROP SCHEMA [calvinlo]
DROP SCHEMA [dqiu]
DROP SCHEMA [DSX Update]
DROP SCHEMA [mpal]
DROP USER [calvinlo]
DROP USER [mpal]
DROP USER [dqiu]
DROP USER [DSX Update]
DROP USER [BUSSOPS\sadmin]
GO
USE [DSX-Update]
DROP SCHEMA [calvinlo]
DROP SCHEMA [dqiu]
DROP SCHEMA [DSX Update]
DROP SCHEMA [mpal]
DROP USER [calvinlo]
DROP USER [mpal]
DROP USER [dqiu]
DROP USER [DSX Update]
DROP USER [BUSSOPS\sadmin]
GO
IF NOT EXISTS (SELECT name FROM master.dbo.syslogins WHERE name = 'acms')
	BEGIN
		CREATE LOGIN [acms] WITH PASSWORD='c?aspa!a!uYe'
	END
ELSE
	BEGIN
		DROP LOGIN [acms]
		CREATE LOGIN [acms] WITH PASSWORD='c?aspa!a!uYe'
	END
GO
USE [AcsData]
IF DATABASE_PRINCIPAL_ID('acms-user') IS NULL
	BEGIN
		CREATE ROLE [acms-user]
	END
IF SCHEMA_ID('acms') IS NULL
	BEGIN
		EXEC ('CREATE SCHEMA [acms]')
	END
IF DATABASE_PRINCIPAL_ID('acms') IS NULL
	BEGIN
		CREATE USER [acms] FROM LOGIN [acms] WITH DEFAULT_SCHEMA = [acms]
	END
EXEC sp_addrolemember N'acms-user', N'acms'
GRANT SELECT ON [dbo].[ACL] TO [acms-user]
GRANT SELECT ON [dbo].[AclGrp] TO [acms-user]
GRANT SELECT ON [dbo].[AclGrpCombo] TO [acms-user]
GRANT SELECT ON [dbo].[AclGrpName] TO [acms-user]
GRANT SELECT ON [dbo].[AclName] TO [acms-user]
GRANT SELECT ON [dbo].[AclProfile] TO [acms-user]
GRANT SELECT ON [dbo].[CARDS] TO [acms-user]
GRANT SELECT ON [dbo].[COMPANY] TO [acms-user]
GRANT SELECT ON [dbo].[DGRP] TO [acms-user]
GRANT SELECT ON [dbo].[FACIL] TO [acms-user]
GRANT SELECT ON [dbo].[HOL] TO [acms-user]
GRANT SELECT ON [dbo].[LOC] TO [acms-user]
GRANT SELECT ON [dbo].[LocCards] TO [acms-user]
GRANT SELECT ON [dbo].[LocGrp] TO [acms-user]
GRANT SELECT ON [dbo].[LocProfile] TO [acms-user]
GRANT SELECT ON [dbo].[NAMES] TO [acms-user]
GRANT SELECT ON [dbo].[OgrpName] TO [acms-user]
GRANT SELECT ON [dbo].[OLL] TO [acms-user]
GRANT SELECT ON [dbo].[OllName] TO [acms-user]
GRANT SELECT ON [dbo].[TZ] TO [acms-user]
GRANT SELECT ON [dbo].[UDF] TO [acms-user]
GRANT SELECT ON [dbo].[UdfName] TO [acms-user]
GO

IF NOT EXISTS (SELECT name FROM master.dbo.syslogins WHERE name = 'DSX User')
	BEGIN
		CREATE LOGIN [DSX User] WITH PASSWORD='UBC2-5276dsx'
	END
ELSE
	BEGIN
		DROP LOGIN [DSX User]
		CREATE LOGIN [DSX User] WITH PASSWORD='UBC2-5276dsx'
	END
GO
USE [AcsData]
IF SCHEMA_ID('DSX User') IS NULL
BEGIN
	EXEC ('CREATE SCHEMA [DSX User]')
END
GO
IF DATABASE_PRINCIPAL_ID('DSX User') IS NULL
BEGIN
	CREATE USER [DSX User] FROM LOGIN [DSX User] WITH DEFAULT_SCHEMA = [DSX User]
END
EXEC sp_addrolemember N'db_datareader', N'DSX User'
EXEC sp_addrolemember N'db_datawriter', N'DSX User'
GO
USE [AcsLog]
IF SCHEMA_ID('DSX User') IS NULL
BEGIN
	EXEC ('CREATE SCHEMA [DSX User]')
END
GO
IF DATABASE_PRINCIPAL_ID('DSX User') IS NULL
BEGIN
	CREATE USER [DSX User] FROM LOGIN [DSX User] WITH DEFAULT_SCHEMA = [DSX User]
END
GO
EXEC sp_addrolemember N'db_datareader', N'DSX User'
EXEC sp_addrolemember N'db_datawriter', N'DSX User'
GO
USE [master]
IF SCHEMA_ID('DSX User') IS NULL
BEGIN
	EXEC ('CREATE SCHEMA [DSX User]')
END
GO
IF DATABASE_PRINCIPAL_ID('DSX User') IS NULL
BEGIN
	CREATE USER [DSX User] FROM LOGIN [DSX User] WITH DEFAULT_SCHEMA = [DSX User]
END
GO