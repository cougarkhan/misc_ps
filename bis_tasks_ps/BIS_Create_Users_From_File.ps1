###############################################################################################################
## CREATE USER FUNCTION
###############################################################################################################
function CreateUser {
    param([string]$strFirstName,[string]$strLastName,[string]$strSamID,[int]$intDept)

    ###############################################################################################################
    ## Declare Constants
    ###############################################################################################################

    Set-Variable -name SUPMAN -value 1 -option constant
    Set-Variable -name PARKING -value 2 -option constant
    Set-Variable -name CSECADMIN -value 3 -option constant
    Set-Variable -name CSECSTAFF -value 4 -option constant
    Set-Variable -name SECACC -value 5 -option constant

    ###############################################################################################################
    ## Check if User already Exists
    ###############################################################################################################

    [string]$strName = "$strFirstName $strLastName"

    [string]$strFilter = "(&(objectCategory=User)(CN=$strName))"

    $objOU = New-Object System.DirectoryServices.DirectoryEntry
    $objSearcher = New-Object System.DirectoryServices.DirectorySearcher
    	
    $objSearcher.SearchRoot = $objOU
    $objSearcher.PageSize = 1000
    $objSearcher.Filter = $strFilter
    $objSearcher.SearchScope = "Subtree"
    	
    $colResults = $objSearcher.FindAll()
       	
    If ($colResults.Count -gt 0) { 
    	Write-Host "Warning!!  " -nonewline -foreground "Yellow"
        Write-Host "User already exists!" -foregroundcolor red
    	Foreach ($objResult in $colResults) {
    		$objItem = $objResult.Properties;
    		$objItem
    	}
        Return 1
    	Exit
    }
    Else {
        Write-Host "`n`nCreating user " -nonewline
        Write-Host "$strFirstName $strLastName" -nonewline -foregroundcolor "Cyan"
        Write-Host "... `n"
    }

    ###############################################################################################################
    ## Prepare User info to enter into AD
    ###############################################################################################################

    [string]$strPassword = "good2go"
    [string]$strUPN = "$strSamID@bussops.ubc.ca"
    [string]$strCountry = "CA"

    Switch ($intDept) {
        $SUPMAN {
            # Supply Management
            [string]$strOU = "OU=Users - Supply Management,OU=Supply Management,DC=bussops,DC=ubc,DC=ca"
            
            [string]$strProfilePath = "\\bops-file1\SUPMANprofiles\$strSamID"
            [string]$strLoginScript = "SUPMANlogin.vbs"
            [string]$strHomeDrive = "P:"
            [string]$strHomeDirectory = "\\bops-file1\SUPMANusers\$strSamID"

            $arrGroupName = , "CN=Supply Management Users,OU=Users - Supply Management,OU=Supply Management,DC=bussops,DC=ubc,DC=ca"

            [string]$strDescription = "Supply Management User"

            #[string]$strManager = "Dennis Silva"
            
            [string]$strEmail = ("$strFirstName.$strLastName@ubc.ca")

            [string]$strOffice = "General Services Administration Building. 1st Floor"
            [string]$strDepartment = "Supply Management"
            [string]$strCompany = "University of British Columbia"

            [string]$strStreetAddress = "#101-2075 Wesbrook Mall"
            [string]$strCity = "Vancouver"
            [string]$strProvince = "British Columbia"
            [string]$strPostalCode = "V6T 1Z1"
            [string]$strTelephone = "604-822-2686"
            [string]$strFax = "604-822-6949"
            [string]$strHomePage = "www.supplymanagement.ubc.ca"
        }
        $PARKING {
            # Parking
            [string]$strOU = "OU=Users-Parking,OU=Parking,DC=bussops,DC=ubc,DC=ca"
            
            [string]$strProfilePath = "\\bops-file1\PACprofiles\$strSamID"
            [string]$strLoginScript = "PAC2-CheckAlreadyLoggedIn.bat"
            [string]$strHomeDrive = "P:"
            [string]$strHomeDirectory = "\\bops-file1\PACusers\$strSamID"

            $arrGroupName = , "CN=PAC-Users,OU=Users-Parking,OU=Parking,DC=bussops,DC=ubc,DC=ca"

            [string]$strDescription = "Parking and Access Control User"

            #[string]$strManager = "Daniel Ho"
            
            [string]$strEmail = ("$strSamID@exchange.ubc.ca")

            [string]$strOffice = "General Services Adminsitration Building. 2nd Floor"
            [string]$strDepartment = "Parking and Access Control Services"
            [string]$strCompany = "University of British Columbia"

            [string]$strStreetAddress = "#204-2075 Wesbrook Mall"
            [string]$strCity = "Vancouver"
            [string]$strProvince = "British Columbia"
            [string]$strPostalCode = "V6T 1Z1"
            [string]$strTelephone = "604-822-6786"
            [string]$strFax = "000-000-0000"
            [string]$strHomePage = "www.parking.ubc.ca"
        }
        $CSECADMIN {
            # Campus Security Admin
            [string]$strOU = "OU=OpsAdmin,OU=Users - Campus Security,OU=Campus Security,DC=ubc,DC=ca"
            
            [string]$strProfilePath = "\\bops-file1\CSECprofiles\$strSamID"
            [string]$strLoginScript = "CSEClogin.vbs"
            [string]$strHomeDrive = "P:"
            [string]$strHomeDirectory = "\\bops-file1\CSECusers\$strSamID"

            $arrGroupName = "CN=CS-ALL,OU=Users - Campus Security,OU=Campus Security,DC=bussops,DC=ubc,DC=ca",`
                                    "CN=CS-OpsAdmin,OU=Users - Campus Security,OU=Campus Security,DC=bussops,DC=ubc,DC=ca",`
                                    "CN=CS-Xerox,OU=Users - Campus Security,OU=Campus Security,DC=bussops,DC=ubc,DC=ca",`
                                    "CN=CS-Staff,OU=Users - Campus Security,OU=Campus Security,DC=bussops,DC=ubc,DC=ca"
            
            [string]$strDescription = "Campus Security Ops Admin User"

            #[string]$strManager = "Tony Mahon"
            
            [string]$strEmail = ("$strFirstName.$strLastName@ubc.ca")

            [string]$strOffice = "Campus Security Building"
            [string]$strDepartment = "Campus Security - Vancouver"
            [string]$strCompany = "University of British Columbia"

            [string]$strStreetAddress = "2133 East Mall"
            [string]$strCity = "Vancouver"
            [string]$strProvince = "British Columbia"
            [string]$strPostalCode = "V6T 1Z4"
            [string]$strTelephone = "604-822-8609"
            [string]$strFax = "604-822-3541"
            [string]$strHomePage = "www.security.ubc.ca"
        }
        $CSECSTAFF {
            # Campus Security Patrol
            [string]$strOU = "OU=Staff,OU=Users - Campus Security,OU=Campus Security,DC=bussops,DC=ubc,DC=ca"
            
            [string]$strProfilePath = "\\bops-file1\CSECprofiles\$strSamID"
            [string]$strLoginScript = "CSEClogin.vbs"
            [string]$strHomeDrive = "P:"
            [string]$strHomeDirectory = "\\bops-file1\CSECusers\$strSamID"

            $arrGroupName = "CN=CS-ALL,OU=Users - Campus Security,OU=Campus Security,DC=bussops,DC=ubc,DC=ca",`
                                    "CN=CS-DefaultHP,OU=Users - Campus Security,OU=Campus Security,DC=bussops,DC=ubc,DC=ca",`
                                    "CN=CS-Staff,OU=Users - Campus Security,OU=Campus Security,DC=bussops,DC=ubc,DC=ca"
            
            [string]$strDescription = "Campus Security User"

            #[string]$strManager = "Tony Mahon"
            
            [string]$strEmail = ("$strSamID@exchange.ubc.ca")

            [string]$strOffice = "Campus Security Building"
            [string]$strDepartment = "Campus Security - Vancouver"
            [string]$strCompany = "University of British Columbia"

            [string]$strStreetAddress = "2133 East Mall"
            [string]$strCity = "Vancouver"
            [string]$strProvince = "British Columbia"
            [string]$strPostalCode = "V6T 1Z4"
            [string]$strTelephone = "604-822-8609"
            [string]$strFax = "604-822-3541"
            [string]$strHomePage = "www.security.ubc.ca"
        }
        $SECACC {
            # Secure Access
            [string]$strOU = "OU=Users - Secure Access,OU=Secure Access,DC=bussops,DC=ubc,DC=ca"
            
            [string]$strProfilePath = "\\bops-file1\SECACCprofiles\$strSamID"
            [string]$strLoginScript = "SECACClogin.vbs"
            [string]$strHomeDrive = "P:"
            [string]$strHomeDirectory = "\\bops-file1\SECACCusers\$strSamID"

            $arrGroupName = ,"CN=Secure Access Users,OU=Users - Secure Access,OU=Secure Access,DC=bussops,DC=ubc,DC=ca"
            
            [string]$strDescription = "Campus Security Ops Admin User"

            #[string]$strManager = "John Molnar"
            
            [string]$strEmail = ("$strSamID@exchange.ubc.ca")

            [string]$strOffice = "Campus Security Building"
            [string]$strDepartment = "Secure Access Employee"
            [string]$strCompany = "University of British Columbia"

            [string]$strStreetAddress = "2133 East Mall"
            [string]$strCity = "Vancouver"
            [string]$strProvince = "British Columbia"
            [string]$strPostalCode = "V6T 1Z4"
            [string]$strTelephone = "604-822-5276"
            [string]$strFax = "604-822-2885"
            [string]$strHomePage = "www.security.ubc.ca"
        }
        Default {
            # No valid department entered.  Exit Script
            Write-Host "No Department specified.  Exiting..."
            Return 1
            Exit
        }
    }


    ###############################################################################################################
    ## Create User
    ###############################################################################################################

    trap [System.Management.Automation.MethodInvocationException] { Write-Host ("ERROR: " + $_) -Foregroundcolor Red; continue }

    $objDE = New-Object System.DirectoryServices.DirectoryEntry("LDAP://$strOU")

    $objUser = $objDE.Create("User","CN=$strName")

    $objUser.put("samaccountname",$strSamID)
    $objUser.put("profilepath",$strProfilePath)
    $objUser.put("scriptpath",$strLoginScript)
    $objUser.put("homedrive",$strHomeDrive)
    $objUser.put("homedirectory",$strHomeDirectory)

    $objUser.put("description",$strDescription)

    $objUser.put("givenname",$strFirstName)
    $objUser.put("sn",$strLastName)
    $objUser.put("cn",$strName)
    $objUser.put("displayname",$strName)
    $objUser.put("name",$strName)
    $objUser.put("userprincipalname",$strUPN)
    $objUser.put("mail",$strEmail.ToLower())

    $objUser.put("physicaldeliveryofficename",$strOffice)
    $objUser.put("department",$strDepartment)
    $objUser.put("company",$strCompany)

    #$objUser.put("manager",$strManager)

    $objUser.put("streetaddress",$strStreetAddress)
    $objUser.put("l",$strCity)
    $objUser.put("st",$strProvince)
    $objUser.put("c",$strCountry)
    $objUser.put("postalcode",$strPostalCode)

    $objUser.put("telephonenumber",$strTelephone)
    $objUser.put("facsimiletelephonenumber",$strFax)
    $objUser.put("wwwhomepage",$strHomePage)

    $objUser.put("userpassword",$strPassword)

    $objUser.setInfo()

    # Add user to groups
    foreach ($i in $arrGroupName) {
        $objGroup = [ADSI]"LDAP://$i"
        $members = $objGroup.member
        $objGroup.member = $members+$objUser.distinguishedname
        $objGroup.setInfo()
        Write-Host "Add " -nonewline
        Write-Host $objUser.name -foregroundcolor "Cyan" -nonewline
        Write-Host " to group " -nonewline
        Write-Host $objGroup.name -foregroundcolor "Yellow" -nonewline
        Write-Host " : " -nonewline
        Write-Host "Success`n" -foregroundcolor "Green"
        $objGroup = $null
    }

    # Enable user account
    $objUser.psbase.invokeset("accountdisabled","False")
    $objUser.setInfo()
    #$objUser.invokeset("setpassword",$strPassword)
}

###############################################################################################################
## CREATE FOLDERS FUNCTION
###############################################################################################################
Function CreateFolders {
    param([string]$strName,[string]$strSamID,[string]$strServer,[string]$strDrive,[string]$strFolder)

    ###############################################################################################################
    ## Check if User already Exists
    ###############################################################################################################

    [string]$strFilter = "(&(objectCategory=User)(CN=$strName))"

    $objOU = New-Object System.DirectoryServices.DirectoryEntry
    $objSearcher = New-Object System.DirectoryServices.DirectorySearcher
    	
    $objSearcher.SearchRoot = $objOU
    $objSearcher.PageSize = 1000
    $objSearcher.Filter = $strFilter
    $objSearcher.SearchScope = "Subtree"
    	
    $colResults = $objSearcher.FindAll()
       	
    If ($colResults.Count -gt 0) { 
    	Foreach ($objResult in $colResults) {
    		$objItem = $objResult.Properties;
    		#$objItem
    	}
        #Write-Host "`nFound User " -nonewline
        #Write-Host "$strName."
    }
    Else {
    	Return 1
    }

    ###############################################################################################################
    ## Create Folders
    ###############################################################################################################

    ###############################################################################################################
    ## Function CreateFolder creates the Profile and User folder
    ## 
    ## $strPath - Path to folder to create and modify
    ## $strNTAccount - Sam ID of the account that will be the owner of the folder
    ###############################################################################################################

    Function CreateFolder{
        param([string]$strPath,[string]$strNTAccount)
        
        ### Check if folder already exists.  If it does exit Function. ###    
        if(!(Test-Path -path $strPath)) {
            
            if(New-Item $strPath -type directory) {
                Write-Host "Create $strPath .... " -nonewline
                Write-Host "Success`n" -foregroundcolor "Green"
            }
            Else {
                Write-Host "Create $strPath .... " -nonewline
                Write-Host "Failure`n" -foregroundcolor "Red"
            }
            
            ### Remove Current Permissions ###
            Write-Host "Getting ACL's for $objACL"
            $objACL = Get-Acl $strPath
            
            foreach ($a in $objACL) { 
                $disableInheritence=$true
                $copyExistingPermissions=$false
                $a.SetAccessRuleProtection($disableInheritence,$copyExistingPermissions)
                $a.ToString()
            }
            
            $objACL.Access
            
            #Set-Acl $strPath $objACL
            
            
            ### Set New Permissions ###
            #$objACL2 = Get-Acl $strPath
            
            $objAdminColRights = [System.Security.AccessControl.FileSystemRights]"FullControl" 
            $objAdminInheritanceFlag = [System.Security.AccessControl.InheritanceFlags]"ContainerInherit,ObjectInherit"
            $objAdminPropagationFlag = [System.Security.AccessControl.PropagationFlags]::None
            $objAdminType =[System.Security.AccessControl.AccessControlType]::Allow
            $objAdminUser = New-Object System.Security.Principal.NTAccount("BUSSOPS\Domain Admins")
            $objAdminACE = New-Object System.Security.AccessControl.FileSystemAccessRule($objAdminUser, $objAdminColRights, $objAdminInheritanceFlag, $objAdminPropagationFlag, $objAdminType)
            
            $objColRights = [System.Security.AccessControl.FileSystemRights]"FullControl" 
            $objInheritanceFlag = [System.Security.AccessControl.InheritanceFlags]"ContainerInherit,ObjectInherit"
            $objPropagationFlag = [System.Security.AccessControl.PropagationFlags]::None
            $objType =[System.Security.AccessControl.AccessControlType]::Allow
            $objUser = New-Object System.Security.Principal.NTAccount("BUSSOPS\$strNTAccount")
            $objACE = New-Object System.Security.AccessControl.FileSystemAccessRule($objUser, $objColRights, $objInheritanceFlag, $objPropagationFlag, $objType)
            
            $objACL.AddAccessRule($objAdminAce)
            $objACL.AddAccessRule($objAce)
            $objACL.SetOwner($objUser)
            
            Set-Acl $strPath $objACL
            
            Write-Host "Permissions : "
            $objACL.Access
            #$objACL2.Access
            Write-Host "`n`n"
            
        }
        Else {
            Write-Host "Warning!!  " -nonewline -foregroundcolor "Yellow"
            Write-Host "$strPath already exists!"
        }
    }

    ### Create Folder ###
    Write-Host "Creating folders for " -nonewline
    Write-Host $strName -nonewline -foregroundcolor "Cyan"
    Write-Host " ... "
    
    $objPing = New-Object System.Net.NetworkInformation.Ping
    $objReply = $objPing.send("$strServer")

    trap { continue }

    If ($objReply.status -eq "Success") {

        CreateFolder "\\$strServer\$strDrive`$\$strFolder\$strSamID" "$strSamID"
    }
    Else {
        Write-Host "File Server not found"
    }
}

###############################################################################################################
## FIRST NAME CREATION FUNCTION
###############################################################################################################
Function FirstName {
    param([string]$strName)
    
    $arrName = $null
    $arrName = $strName.Split()
    
    Return $arrName[0].Trim()
}

###############################################################################################################
## LAST NAME CREATION FUNCTION
###############################################################################################################
Function LastName {
    param([string]$strName)
    
    $arrName = $strName.Split()
    $arrName[0] = $null
    $strLastName = [string]::Join(" ",$arrName)
    Return $strLastName.Trim()
}

###############################################################################################################
## SAMID CREATION FUNCTION
###############################################################################################################
Function MakeSAMID {
    param([string]$strName)
    
    $arrName = $null
    $arrName = $strName.Split()
    $strFirstInitial = $arrName[0].Substring(0,1).ToLower().Trim()
    $strLastName = LastName($strName)
    $strLastName = $strLastName.ToLower().Trim() -replace(" ","")
    
    Return $strFirstInitial + $strLastName
}



Function StartCreateUserProcess {
    param([string] $strFullName, [int] $intDept)


    ###############################################################################################################
    ## Declare Constants
    ###############################################################################################################

    Set-Variable -name SUPMAN -value 1 -option constant
    Set-Variable -name PARKING -value 2 -option constant
    Set-Variable -name CSECADMIN -value 3 -option constant
    Set-Variable -name CSECSTAFF -value 4 -option constant
    Set-Variable -name SECACC -value 5 -option constant

    ###############################################################################################################
    ## Get User Input
    ###############################################################################################################

    Write-Host "Please Select from the following Departments`n"
    Write-Host "1. Supply Management"
    Write-Host "2. Parking and Access Control"
    Write-Host "3. Campus Security Admin"
    Write-Host "4. Campus Security Staff"
    Write-Host "5. Secure Access"
    Write-Host "`n0. Exit"

    #[int]$intDept = Read-Host "`nEnter a number corresponding with a Department"

    while ($intDept -lt 0 -AND $intDept -gt 5) {
        Write-Host "Please Select from the following Departments`n"
        Write-Host "1. Supply Management"
        Write-Host "2. Parking and Access Control"
        Write-Host "3. Campus Security Admin"
        Write-Host "4. Campus Security Staff"
        Write-Host "5. Secure Access"
        Write-Host "`n0. Exit"

        #[int]$intDept = "`nEnter a number corresponding with a Department"
    }

    #$strFullName = Read-Host "Enter a First and Last Name"
    $strFirstName = FirstName($strFullName)
    $strLastName = LastName($strFullName)
    $strSamID = MakeSamID($strFullName)

    $strFirstName = [System.Text.RegularExpressions.Regex]::Replace($strFirstName,'[^1-9a-zA-Z_]','')
    $strLastName = [System.Text.RegularExpressions.Regex]::Replace($strLastName,'[^1-9a-zA-Z_ ]','')
    $strFullName = "$strFirstName $strLastName"

    $strFirstName
    $strLastName
    $strSamID

    while ($strFullName -eq "" -OR $strFirstName -eq "" -OR $strLastName -eq "") {
        $strFullName = Read-Host "Enter a First and Last Name"
        $strFirstName = FirstName($strFullName)
        $strLastName = LastName($strFullName)
        $strSamID = MakeSamID($strFullName)
    }

    ###############################################################################################################
    ## Set Department specific variables
    ###############################################################################################################

    Switch($intDept) {
        $SUPMAN {
            $strServer = "bops-file1"
            $strDrive = "e"
            $strProfileFolder = "SUPMAN\SUPMANprofiles"
            $strHomeFolder = "SUPMAN\SUPMANusers"
        }
        $PARKING {
            $strServer = "bops-file1"
            $strDrive = "e"
            $strProfileFolder = "PARKING\PACprofiles"
            $strHomeFolder = "PARKING\PACUsers"
        }
        $CSECADMIN {
            $strServer = "bops-file1"
            $strDrive = "e"
            $strProfileFolder = "CSEC\CSECprofiles"
            $strHomeFolder = "CSEC\CSECusers"
        }
        $CSECSTAFF {
            $strServer = "bops-file1"
            $strDrive = "e"
            $strProfileFolder = "CSEC\CSECprofiles"
            $strHomeFolder = "CSEC\CSECusers"
        }
        $SECACC {
            $strServer = "bops-file1"
            $strDrive = "e"
            $strProfileFolder = "SECACC\SECACCprofiles"
            $strHomeFolder = "SECACC\SECACCusers"
        }
        0 {
            Write-Host "Good-Bye!"
            Exit
        }
        Default {
            Write-Host "What?"
            Exit
        }
    }

    ###############################################################################################################
    ## Create User and Folders
    ###############################################################################################################
    CreateUser $strFirstName $strLastName $strSamID $intDept
    CreateFolders $strFullName $strSamID $strServer $strDrive $strProfileFolder
    CreateFolders $strFullName $strSamID $strServer $strDrive $strHomeFolder
}

StartCreateUserProcess "itsm user 1" 1
StartCreateUserProcess "itsm user 2" 2
StartCreateUserProcess "itsm user 3" 3
StartCreateUserProcess "itsm user 4" 4
StartCreateUserProcess "itsm user 5" 5