$arrFiles = Get-ChildItem -path C:\Temp\PAC_Desktops -recurse -force | where {$_.Extension -match "csv"}

Write-Host arrFiles has $arrFiles.Count elements.

$arrArrayOfApps = gc $arrFiles[0].FullName
Remove-Item C:\Temp\temp.csv
$arrArrayOfApps >> C:\Temp\temp.csv

For ($x=1; $x -lt $arrFiles.Count; $x++) {
    Write-Host Iteration \# $x `n -foregroundcolor "Red"
    
    $arrArrayOfApps = gc C:\Temp\temp.csv

    Write-Host Reading contents from $arrFiles[$x].FullName ... -nonewline
    $arrApp = gc $arrFiles[$x].FullName
    Write-Host Done! `n
    
    Write-Host Comparing objects ... -nonewline
    $arrResult = compare-object $arrArrayOfApps $arrApp | Where-Object {$_.SideIndicator -eq '=>'}
    Write-Host Done! `n
    
    Foreach ($app in $arrResult) {
        Write-Host Entering the following into the Array List:" " -nonewline
        Write-Host $app.InputObject -foregroundcolor "Green"
        $app.InputObject >> C:\Temp\temp.csv
    }
}

Remove-Item "P:\Projects\PAC Desktop Refresh\Applications_List.txt"
Remove-Item "P:\Projects\PAC Desktop Refresh\Applications_List_Per_Machine.txt"

Import-Csv C:\Temp\temp.csv | Sort-Object Name | Format-Table Name,Version,Vendor -Autosize >> "P:\Projects\PAC Desktop Refresh\Applications_List_Unique.txt"

Foreach ($pc in $arrFiles) {
    $strHeader = "`n`n############################################`n"
    $strHeader += "              " + $pc.BaseName + "`n"
    $strHeader += "############################################`n`n"
    $strHeader >> "P:\Projects\PAC Desktop Refresh\Applications_List_Per_Machine.txt"
    Import-Csv $pc.FullName | Sort-Object Name | Format-Table Name,Version,Vendor -Autosize >> "P:\Projects\PAC Desktop Refresh\Applications_List_Per_Machine.txt"
}

#$arrAppsList = gc C:\Temp\temp.txt -delimiter "'"
#for ($x=0;$x -lt $arrAppsList.count;$x++) {$arrAppsList[$x] = $arrAppsList[$x].Replace("`"","")}
#for ($x=0;$x -lt $arrAppsList.count;$x++) {$arrAppsList[$x] = $arrAppsList[$x].Replace(",","")}
