###########################################################################
# Environment Variables
###########################################################################
$strUserName = gc env:username

###########################################################################
# Login Tracking File
###########################################################################
$strLoginFile = "\\bops-file1\supmanlogin$\" & $strUserName & ".txt"

###########################################################################
# Delete Drive Mappings
###########################################################################
net share Q: /d /y

###########################################################################
# Check if the login tracking file exists.  If it does exist then log 
# the user out.  Otherwise log the user in and map the appropriate drives 
# and clean cache files.
###########################################################################
If (test-path $strLoginFile) {
    $objAlert = new-object -comobject wscript.shell
    $arrFileContents = gc $strLoginFile
    $strLogoutWarning = "You are already logged on to the network.`n`n" & $arrFileContents[2] & "System policy does not allow multiple logins.`nYou will be immediatley logged off this computer"
    
    $intButton = $objAlert.popup("test",10,"Test BOx",48)
    
    if ($intButton = 1) {
        




























###########################################################################
# 
###########################################################################