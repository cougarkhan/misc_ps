#**************************************************************************************
# Folder Settings
#**************************************************************************************
$logFolder = "C:\Apache\Apache2.2\logs"
$backupFolder = "C:\Apache\logs-backup"
$backupFolder2 = "\\bops-file1\JobLogs"

#**************************************************************************************
# Get list of log files
#**************************************************************************************
$logFiles = gci $logFolder | where-object {$_.extension -notmatch "pid"}

#**************************************************************************************
# Format Date Strings
#**************************************************************************************
$seconds = (get-date).Second
if ($seconds.length -eq 1) { $seconds = "0$seconds" }

$minute = (get-date).Minute
if ($minute.length -eq 1) { $minute = "0$minute" }

$hour = (get-date).Hour
if ($hour.length -eq 1) { $hour = "0$hour" }

$day = (get-date).day.tostring()
if ($day.length -eq 1) { $day = "0$day" }

$month = (get-date).month.tostring()
if ($month.length -eq 1) { $month = "0$month" }

$year = (get-date).year.tostring()

$backupDate = "$year$month$day"

#**************************************************************************************
# Copy files over and rename them with date.  Create new empty log files.
#**************************************************************************************
foreach ($file in $logFiles) {
    if (test-path $backupFolder\$backupDate-$file.Name) {
        mv $file.FullName $backupFolder\$backupDate-$file.Name-2
    } else {
        mv $file.FullName $backupFolder\$backupDate-$file.Name
    }
    ni -path $file.FullName -type file
}