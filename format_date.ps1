﻿function format_date {
	# Format Date Strings
	$day = (get-date).day.tostring()
	if ($day.length -eq 1) { $day = "0$day" }

	$month = (get-date).month.tostring()
	if ($month.length -eq 1) { $month = "0$month" }

	$year = (get-date).year.tostring()


	return "$year$month$day"
}

function format_time {
	#Format Time Strings
	$seconds = (get-date).Second
	if ($seconds.length -eq 1) { $seconds = "0$seconds" }

	$minute = (get-date).Minute
	if ($minute.length -eq 1) { $minute = "0$minute" }

	$hour = (get-date).Hour
	if ($hour.length -eq 1) { $hour = "0$hour" }
}