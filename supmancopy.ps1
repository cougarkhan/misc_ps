# initialize the items variable with the
# contents of a directory
$items = Get-ChildItem -Path "\\businessop\f$\SM-Profiles"
$folders = @()

# enumerate the items array
foreach ($item in $items)
{
      # if the item is a directory, then process it.
      if ($item.Attributes -eq "Directory")
      {
             $folders = $folders + $item.Name
      }
}

