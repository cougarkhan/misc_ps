#$strPath = "C:\Temp\CSEC\profiles2"
$strPath = Read-Host "Enter Folder Path..."
$arrFolders = Get-ChildItem $strPath -recurse -force | where {$_.Extension -match "dat"}
$arrEntries = @(,,"")
$intPMeter1 = $arrFolders.Count

$p1 = 0

Foreach ($folder in $arrFolders) {
    $task1_progress = [int][Math]::Ceiling((($p1 / $intPMeter1) * 100))
    Write-Progress -Activity "Checking Registries" -PercentComplete $task1_progress -Status "Processing Registries - $task1_progress%" -Id 2 -CurrentOperation $folder.FullName
    
    #Write-Host `nLoading... $folder.FullName -foregroundcolor "GREEN"
    REG LOAD HKLM\TEMP $folder.FullName | out-null
    REG QUERY HKLM\TEMP\Network > c:\Temp\drives.txt
    
    $arrDrives = Select-String c:\Temp\drives.txt -pattern "HKEY_LOCAL_MACHINE\\TEMP\\Network\\"
    
    $p2 = 0
    $intPMeter2 = $arrDrives.Count

    Foreach ($line in $arrDrives) {
        If ($intPMeter2 -lt 1) { $intPMeter2 = 1 }
        
        $task2_progress = [int][Math]::Ceiling((($p2 / $intPMeter2) * 100))
        Write-Progress -Activity "Checking Keys" -PercentComplete $task2_progress -Status "Processing Keys - $task2_progress%" -Id 3 -CurrentOperation $line.Line
        
        If ($line -ne $Null) {
            
            REG QUERY $line.Line /v "RemotePath" > c:\Temp\remotepaths.txt
            $arrRemotePaths = @()
            $arrRemotePaths += Select-String c:\Temp\remotepaths.txt -pattern "RemotePath"
            
            
            $p3 = 0
            $intPMeter3 = $arrRemotePaths.Count
                        
            Foreach ($path in $arrRemotePaths) {
                If ($intPMeter3 -lt 1) { $intPMeter3 = 1 }
                
                $task3_progress = [int][Math]::Ceiling((($p3 / $intPMeter3) * 100))
                Write-Progress -Activity "Checking Values" -PercentComplete $task3_progress -Status "Processing Values - $task3_progress%" -Id 4 -CurrentOperation $path.Line
                
                $intSize = $arrEntries.Count
                    
                For ($i=0; $i -lt $intSize; $i++) {
                    If ($arrEntries[$i] -contains $line.Line -AND $arrEntries[$i] -contains $path.Line) {
                        #$arrEntries[$i] += ($line.Line,$path.Line)
                        #Write-Host "Entry  Added! " $arrEntries[$i] -foregroundcolor "CYAN"
                    }
                    ElseIf ($line.Line -imatch "HKEY_LOCAL_MACHINE\\TEMP\\Network\\V"){
                        Write-Host "STUPID V DRIVE!" -foregroundcolor "RED"
                    }
                    Else {
                        $arrEntries[$i] += ($line.Line,$path.Line)
                        Write-Host "Entry Added!" -foregroundcolor "BLUE"
                    }
                }
                $p3++
            }
        }
        $p2++
    }
        
    #Write-Host Unloading... $folder.FullName -foregroundcolor "GREEN"
    REG UNLOAD HKLM\TEMP | out-null
    
    $p1++
}

Write-Host "`nUnique Drive Mappings...`n" -foregroundcolor "GREEN"
$arrEntries