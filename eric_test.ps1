Import-Module ActiveDirectory

function get-users {
	param ([string]$server, [string]$searchBase, [string]$domainUser, [string]$password = (read-host "$domainUser Password" -assecurestring | convertfrom-securestring))
		
	$passSecure = $password | convertto-securestring
	
	$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $domainUser, $passSecure
	
	$accounts = Get-ADUser -LDAPFilter "(enterpriseAccountName=CA*)" -Properties enterpriseAccountName -Server $server -Credential $cred
	
	$accounts | %{
		$ean = $_.enterpriseAccountName
		$san = $_.sAMAccountName 
		$gn = $_.GivenName
		$sn = $_.SurName
        $sn = $sn -replace("",'')
		
		Write-Host "$ean : $san"
	}
}

$domainList1 = get-users <server> <searchBase> <domainUser> 
$domainList2 = get-users <server> <searchBase> <domainUser>
$domainList3 = get-users <server> <searchBase> <domainUser>

$domainList1
$domainList2
$domainList3
