#####################################################################################################################
#
# Script to Alert of HDD Failure
# Log files will be written to \\bops-file1\Server Failures\
#
# Create on 2011-08-26
# by Michael Pal
#
#####################################################################################################################

# Parameters passed to the script

param([string]$from,[string]$to,[string]$subject,[string]$body)

# Server Name
$server = $env:computername

# Format Date Strings
$seconds = (get-date).Second
if ($seconds.length -eq 1) { $seconds = "0$seconds" }

$minute = (get-date).Minute
if ($minute.length -eq 1) { $minute = "0$minute" }

$hour = (get-date).Hour
if ($hour.length -eq 1) { $hour = "0$hour" }

$day = (get-date).day.tostring()
if ($day.length -eq 1) { $day = "0$day" }

$month = (get-date).month.tostring()
if ($month.length -eq 1) { $month = "0$month" }

$year = (get-date).year.tostring()

$failureDate = "$year$month$day"

$logFile = "\\bops-file1\JobLogs\Hardware_Failures\$failureDate$hour-$minute-$seconds-$server-HW_Failure.txt"

# Sender
if (!$from) {
    $from = "bops.systems@exchange.ubc.ca"
}

# Recipients List
if (!$to) {
    $to = "michael.pal@ubc.ca"
}

# Subject and Body
if (!$subject -and !$body) {
    $subject = "Unspecified error or possible hardware failure on $server"
    $body = "Unspecified error or possible hardware failure on $server. `n`n Please activate -subject and -body flags for more informational messages."
} 
elseif (!$subject) {
    $subject = "Unspecified error or possible hardware failure on $server"
    
    # Body
    if (!$body) {
        $body = "Unspecified error or possible hardware failure on $server"
    }
}

# Body
if (!$body) {
    $body = "$subject on $server"
}

# SMTP Server
$smtpServer = "mail-relay.ubc.ca"

# Create new SMTP object to send email
$smtp = new-object Net.Mail.SmtpClient($smtpServer)

# Send the Email
$from >> $logfile
$to >> $logfile
$subject >> $logfile
$body >> $logfile

$smtp.Send($from, $to, $subject, $body)

