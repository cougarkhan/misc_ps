#Check Folders

[xml]$config =  gc .\etc\preflightconfig.xml

# XML Config Options
$folders = $config.config.folders.name
$log = $config.config.log
$network = $config.config.network
$software = $config.config.software

# WMI objects
$networkAdapters = gwmi Win32_NetworkAdapterConfiguration | where { $_.IPEnabled -eq $true } | select *
$machineInfo = gwmi Win32_ComputerSystem | select *
$nacBind = [wmiclass]"Win32_NetworkAdapterConfiguration"

# Create Folders
$folders | %{
	if ((test-path $_.value) -eq $false) {
		new-item -Path $_.value -type directory
	} else {
		Write-Host "$item exists."
	}
}

if ((test-path $log.file) -eq $false) {
	if (new-item -Path $log.file -type file) {
		$create_log = ".LOG"
		$create_log > $log.file
		C:\Windows\System32\cmd /c mklink $log.shortcut $log.file
	}
} else { 
	Write-Host "Could not create log file."
}

[array]$suffixes = $network.dns.suffix

$nacBind.SetDNSuffixSearchOrder($suffixes)

add-computer -domain $network.dns.dnsdomain

netsh advfirewall firewall add rule name="icmp" dir=in action=allow enable=yes protocol=icmpv4 interfacetype=any profile=any

msiexec /i $software.nsclient.filename /quiet /norestart ADDLOCAL="ALL"
stop-service 

msiexec /i $software.7zip.filename /q INSTALLDIR="C:\Program Files\7-Zip"

$software.notepadpp.filename /S