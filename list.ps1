#$strPath = "c:\temp\cs2"
$strPath = Read-Host "Enter Folder Path..."
$arrFolders = Get-ChildItem $strPath -recurse -force | where {$_.Extension -match "dat"}
$arrEntries = @(,,"")
Write-host "arrEntries.Count : " $arrEntries.Count

Foreach ($folder in $arrFolders) {
    Write-Host `n`nLoading... $folder.FullName -foregroundcolor "GREEN"
    REG LOAD HKLM\TEMP $folder.FullName | out-null
    REG QUERY HKLM\TEMP\Network > c:\Temp\drives.txt
    
    $arrDrives = Select-String c:\Temp\drives.txt -pattern "HKEY_LOCAL_MACHINE\\TEMP\\Network\\"
    
    $folder.FullName    
    
    Foreach ($line in $arrDrives) {
        If ($line -ne $Null) {
            REG QUERY $line.Line /v "RemotePath" > c:\Temp\remotepaths.txt
            $arrRemotePaths = Select-String c:\Temp\remotepaths.txt -pattern "RemotePath"
            
            Foreach ($path in $arrRemotePaths) {
                $intSize = $arrEntries.Count
                For ($i=0; $i -lt $intSize; $i++) {
                    If ($arrEntries[$i] -contains $line.Line -AND $arrEntries[$i] -contains $path.Line) {
                        Write-Host "Duplicate Entry!"
                    }
                    Else ($line.Line -imatch "HKEY_LOCAL_MACHINE\\TEMP\\Network\\V"){
                        Write-Host "STUPID V DRIVE!" -foregroundcolor "RED"
                    }
                }
            }
        }
    }
        
    Write-Host Unloading... $folder.FullName -foregroundcolor "GREEN"
    REG UNLOAD HKLM\TEMP | out-null
}

$arrEntries
Write-Host "Huh?"