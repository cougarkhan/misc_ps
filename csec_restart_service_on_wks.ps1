param ([string]$pc)

function restart_service {
param ([string] $computer)

(get-wmiobject -computer $computer win32_service -filter "Name='dnscache'").invokemethod("stopservice",$null)
(get-wmiobject -computer $computer win32_service -filter "Name='dnscache'").invokemethod("startservice",$null)
}

restart_service $pc