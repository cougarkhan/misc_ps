/sbin/iptables -t nat -F REDNAT
/sbin/iptables -t nat -F CUSTOMPREROUTING
/sbin/iptables -t nat -F CUSTOMPOSTROUTING
/sbin/iptables -t filter -F CUSTOMFORWARD
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.8 -p tcp --dport 1433 -d 172.19.10.204 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p tcp --dport 22223 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.151 -p tcp --dport 1433 -d 10.30.2.8 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.151 -p tcp --dport 22223 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5013 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5013 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5013 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5013 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5021 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5021 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5021 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5021 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5025 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5025 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5025 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5025 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5026 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5026 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5026 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5026 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5027 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5027 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5027 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5027 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5028 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5028 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5028 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5028 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5029 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5029 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5029 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5029 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5033 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5033 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5033 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5033 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5034 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5034 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5034 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5034 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5036 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5036 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5036 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5036 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5038 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5038 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5038 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5038 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5041 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5041 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5041 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5041 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5044 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5044 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5044 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5044 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5045 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5045 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5045 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5045 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5046 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5046 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5046 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5046 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5047 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5047 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5047 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5047 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5048 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5048 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5048 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5048 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5049 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5049 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5049 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5049 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5051 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5051 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5051 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5051 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5052 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5052 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5052 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5052 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5053 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5053 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5053 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5053 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5056 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5056 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5056 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5056 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5057 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5057 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5057 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5057 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5059 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5059 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5059 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5059 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5060 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5060 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5060 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5060 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5061 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5061 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5061 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5061 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5062 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5062 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5062 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5062 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5063 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5063 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5063 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5063 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5064 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5064 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5064 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5064 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5065 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5065 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5065 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5065 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5066 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5066 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5066 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5066 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5067 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5067 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5067 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5067 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5068 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5068 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5068 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5068 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5069 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5069 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5069 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5069 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5070 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5070 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5070 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5070 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5071 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5071 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5071 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5071 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5072 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5072 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5072 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5072 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5073 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5073 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5073 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5073 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5074 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5074 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5074 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5074 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5075 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5075 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5075 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5075 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5076 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5076 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5076 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5076 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5077 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5077 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5077 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5077 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5078 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5078 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5078 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5078 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5079 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5079 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5079 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5079 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5080 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5080 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5080 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5080 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5082 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5082 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5082 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5082 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5083 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5083 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5083 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5083 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5084 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5084 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5084 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5084 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5085 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5085 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5085 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5085 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5086 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5086 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5086 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5086 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5087 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5087 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5087 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5087 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5088 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5088 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5088 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5088 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5089 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5089 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5089 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5089 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5090 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5090 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5090 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5090 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5091 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5091 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5091 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5091 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5093 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5093 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5093 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5093 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5094 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5094 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5094 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5094 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5095 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5095 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5095 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5095 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5096 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5096 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5096 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5096 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5098 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5098 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5098 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5098 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5100 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5100 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5100 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5100 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5101 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5101 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5101 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5101 -d 172.19.10.98
/sbin/iptables -t nat -I CUSTOMPREROUTING -i lan-1 -j DNAT --to-destination 10.30.2.9 -p udp --dport 5111 -d 172.19.10.205 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5111 -d 10.30.2.9 -s 172.19.10.0/24
/sbin/iptables -t nat -I CUSTOMPREROUTING -i wan-1 -j DNAT --to-destination 172.19.10.98 -p udp --dport 5111 -d 142.103.74.154 -s 10.30.2.9
/sbin/iptables -t nat -I CUSTOMPOSTROUTING -o wan-1 -j SNAT --to-source 142.103.74.154 -p udp --dport 5111 -d 172.19.10.98
