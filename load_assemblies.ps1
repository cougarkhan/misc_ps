function load_assemblies {
	param([string]$name)
	$startTime = get-date
	
	Write-Host "Loading .NET SQL Assembly " -nonewline
	Write-Host "$name" -nonewline -foregroundcolor "CYAN"
	Write-host " ... " -nonewline
	if ([System.Reflection.Assembly]::LoadWithPartialName($name)) {
		$endTime = get-date
		Write-Host "Completed." -foregroundcolor "GREEN" -nonewline
		$compTime = ($endTime - $startTime).TotalSeconds
		Write-Host " ($compTime secs)"
	} else {
		$endTime = get-date
		Write-Host "Failed" -foregroundcolor "RED" -nonewline
		$compTime = ($endTime - $startTime).TotalSeconds
		Write-Host " ($compTime secs)"
	}
}