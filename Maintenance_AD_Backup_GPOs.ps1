#####################################################################################################################
#
# Script to backup BUSSOPS GPOs
# Log files will be written to \\bops-file1\JobLogs\Maintenance
#
# Create on 2011-08-08
# by Michael Pal
#
#####################################################################################################################

#Server Name
$server = "THELETTERMAN"

# Format Date Strings
$seconds = (get-date).Second
if ($seconds.length -eq 1) { $seconds = "0$seconds" }

$minute = (get-date).Minute
if ($minute.length -eq 1) { $minute = "0$minute" }

$hour = (get-date).Hour
if ($hour.length -eq 1) { $hour = "0$hour" }

$day = (get-date).day.tostring()
if ($day.length -eq 1) { $day = "0$day" }

$month = (get-date).month.tostring()
if ($month.length -eq 1) { $month = "0$month" }

$year = (get-date).year.tostring()


$backupDate = "$year$month$day"
$backup_destination = "\\bops-file1\BISshare\Backup\GPO\$backupDate"

$logFile = "\\bops-file1\JobLogs\Maintenance\$backupDate$hour-$minute-$seconds-$server-Backup-GPOs.txt"

if ((test-path $backup_destination) -ne $true) {
    new-item $backup_destination -type directory
}

Import-Module GroupPolicy
backup-gpo -all -path $backup_destination -domain "bussops.ubc.ca" > $logfile

exit