##############################################################################################
#
# SQL BACKUP VERIFICATION
#
##############################################################################################
#
# This script will check 3 conditions to see if the MS SQL backup occurred.
# 
# 1. It will check whether the scheduled task for the MS SQL Daily Backup ran successfully.
# 2. It will check to see if there are files from the current script run day in the 
#    specified backup location.
# 3. It will compare the current and last backup file sizes and send an alert if the margin is
#    off by 15% (default) or a margin specified by the -m command
#
# -b    Specifies the backup location to search in for backups.
# -n    The name of the SQL Server
# -m    The margin by which the file size can differ in a comparison
#
##############################################################################################
#
# Script written by Michael Pal on Sept 28th 2011
#
##############################################################################################

param ( [string]$b,
        [string]$n=$Env:COMPUTERNAME )
		
      
. .\functions\send_email.ps1
. .\functions\format_date.ps1


$backupDate = format_date
$backupTime = format_time

        
[array]$errorLog = @()
[array]$backupLog = @()
$backupLogLoc = "\\bops-file1\JobLogs\$backupDate$backupTime-$n-MSSQL_Backup_Verify.txt"

if (!(test-path $backupLogLoc)) {
    new-item $backupLogLoc -type file
}

#####################################################################################
# Did the SQL Agent Jobs run successfully?  If not send an error email.
#####################################################################################
# Load SMO extension
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null;

# Create an SMO Server object
$sqlServer = New-Object "Microsoft.SqlServer.Management.Smo.Server" $n;

foreach ($job in $sqlServer.JobServer.Jobs) {
    if ($job.category -eq "Database Maintenance") {
        $lastRunDate = $job.lastrundate
        $lastRunOutcome = $job.lastrunoutcome
        $name = $job.name
        
        if ($lastRunOutcome.gethashcode() -eq 0) {
			$errorLog += "$name failed on $n at $lastRunDate"
		}
    }
}

#####################################################################################
# Check for the existence of the latest backup.  If not send an error email.
#####################################################################################
gci $b | foreach { if ($_.psiscontainer -eq $true) {
		
		$folder = $_.fullname
		$currentBackups = gci $folder | where {$_.creationtime.dayofyear -eq (get-date).dayofyear}
				
		if (!$currentBackups) {
		    $errorLog += "`nBackup file for " + (get-date).tolongdatestring() + " is missing in folder $folder!  Backup job may not have ran."
		    
		} else {
		    $backupLog += "`nFound the following new backup files:`n"
		    $backupLog += $currentBackups
		}
	}
}

if ($errorLog.length -gt 0) {
	$errorLog | out-file $backupLogLoc -Append
	send_email($errorLog)
}

if ($backupLog.length -gt 0) {
	$backupLog | out-file $backupLogLoc -Append
}

exit