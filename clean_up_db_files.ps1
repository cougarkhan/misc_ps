#####################################################################################################################
#
# Script to clean up database files
# Log files will be written to \\bops-file1\JobLogs
#
# Create on 2011-04-27
#
#####################################################################################################################

# Directory to clean up
$cleanUpDir = "A:\backups"

# Extentions
$cleanUpExts = "BAK|TRN|TXT"

$folderList = get-childitem $cleanUpDir -recurse | where-object {$_.name -like "D3*" -and $_.PSIsContainer -eq $true}
$folderList

# Format Date Strings
$seconds = (get-date).Second
if ($seconds.length -eq 1) { $seconds = "0$seconds" }

$minute = (get-date).Minute
if ($minute.length -eq 1) { $minute = "0$minute" }

$hour = (get-date).Hour
if ($hour.length -eq 1) { $hour = "0$hour" }

$day = (get-date).day.tostring()
if ($day.length -eq 1) { $day = "0$day" }

$month = (get-date).month.tostring()
if ($month.length -eq 1) { $month = "0$month" }

$year = (get-date).year.tostring()

# Assemble date string
$backupDate = "$year$month$day"

# Set how many days of backups to keep
$fileAgeLimit = (get-date).AddDays(1)

# Time strings for logging
$today = (get-date).toshortdatestring()
$time = (get-date).toshorttimestring()

# Set log file name
$logFileName = "$backupDate$hour-$minute-$seconds-BUSSOPSD3-FILE-CLEANUP"

# Reason why file was deleted
$description = "File is older than 2 days and has been deleted."

# Create Log File
#$logFile = "\\bops-file1\JobLogs\$logFileName.txt"
$logFile = "A:\$logFileName.txt"

if (!(test-path $logFile)) {
    new-item $logFile -type file
}


# Delete files that are older than 2 days

# If folders exist continue
if ($folderList) {
    
    # Perform operations on each folder 
    foreach ($folder in $folderList) {
        
        $files = get-childitem $folder.fullname -recurse | where-object {$_.extension -match $cleanUpExts}
        
        foreach ($item in $files) {
            if ($item.CreationTime -lt $fileAgeLimit) {

                    try {
                        if (test-path $item.directory) {
                            #Remove-Item -path $item.directory -recurse
                            #"{0,-9} {1,-12} {2,-30} {3,-28} {4,-20}" -f $today, $time, $item.FullName, $item.CreationTime.ToString(), $description | out-file $logFile -append
                            "$today{0}$time{1,13}$item{2}$description" -f "   ","","   " | out-file $logFile -append
                        }
                    }
                    catch {
                        "`nNo Files to delete`n" >> $logFile
                    }
            }
        }
    }
}