$strPath = "a:\temp\csec\profiles2"
$arrFolders = Get-ChildItem $strPath -recurse -force | where {$_.Extension -match "dat"}

Foreach ($folder in $arrFolders) {
    Write-Host `n`nLoading... $folder.FullName -foregroundcolor "GREEN"
    REG LOAD HKLM\TEMP $folder.FullName
    REG QUERY HKLM\TEMP\Network > a:\Temp\drives.txt
    $arrDrives = Select-String a:\Temp\drives.txt -pattern "HKEY_LOCAL_MACHINE\\TEMP\\Network\\"
    
    Foreach ($line in $arrDrives) {
        If ($line -ne $Null) {
            If ($line.Line -imatch "Network\\V") {
                REG DELETE $line.Line /f
                Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
            }
        }
    }
        
    Write-Host Unloading... $folder.FullName -foregroundcolor "GREEN"
    REG UNLOAD HKLM\TEMP
}