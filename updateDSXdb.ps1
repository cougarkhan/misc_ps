#load assemblies
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null
#Need SmoExtended for backup
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoEnum") | Out-Null
 
#get backup file
#you can also use PowerShell to query the last backup file based on the timestamp
#I'll save that enhancement for later
$backupFile = "E:\AcsData_backup_2011_11_16_180009_5049899.bak"
 
#we will query the db name from the backup file later
 
$server = New-Object ("Microsoft.SqlServer.Management.Smo.Server") "(local)"
$backupDevice = New-Object ("Microsoft.SqlServer.Management.Smo.BackupDeviceItem") ($backupFile, "File")
$smoRestore = new-object("Microsoft.SqlServer.Management.Smo.Restore")
 
#settings for restore
$smoRestore.NoRecovery = $false;
$smoRestore.ReplaceDatabase = $true;
$smoRestore.Action = "Database"
 
#show every 10% progress
$smoRestore.PercentCompleteNotification = 10;
 
$smoRestore.Devices.Add($backupDevice)
 
#read db name from the backup file's backup header
$smoRestoreDetails = $smoRestore.ReadBackupHeader($server)

#display database name
$dbname = $smoRestoreDetails.Rows[0]["DatabaseName"]

"Database Name from Backup Header : " + $dbnamne

$smoRestore.Database = $dbname

# Kill All connections to existing database
$smoserver.killallprocess($dbname)
 
#restore
$smoRestore.SqlRestore($server)