$strPath = "C:\Temp\CSEC\profiles2"
$arrFolders = Get-ChildItem $strPath -recurse -force | where {$_.Extension -match "dat"}

function get_keys {
    Get-ChildItem HKLM:\TEMP\Network | ForEach-Object {Get-ItemProperty $_.pspath}
}

Foreach ($folder in $arrFolders) {
    Write-Host `n`nLoading... $folder.FullName -foregroundcolor "GREEN"
    REG LOAD HKLM\TEMP $folder.FullName
    REG QUERY HKLM\TEMP\Network > c:\Temp\drives.txt
    
    $arrDrives = Select-String c:\Temp\drives.txt -pattern "HKEY_LOCAL_MACHINE\\TEMP\\Network\\"
    Foreach ($line in $arrDrives) {
        If ($line -ne $Null) {
            # Switch ($line.Line) {
                # HKEY_LOCAL_MACHINE\TEMP\Network\V { 
                    # REG DELETE $line.Line /f
                    # Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
                # }
                # HKEY_LOCAL_MACHINE\TEMP\Network\Q { 
                    # REG DELETE $line.Line /f
                    # Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
                # }
                # HKEY_LOCAL_MACHINE\TEMP\Network\R { 
                    # REG DELETE $line.Line /f
                    # Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
                # }
                # HKEY_LOCAL_MACHINE\TEMP\Network\S { 
                    # REG DELETE $line.Line /f
                    # Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
                # }
                # HKEY_LOCAL_MACHINE\TEMP\Network\M { 
                    # REG DELETE $line.Line /f
                    # Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
                # }
                # HKEY_LOCAL_MACHINE\TEMP\Network\N { 
                    # REG DELETE $line.Line /f
                    # Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
                # }
                # HKEY_LOCAL_MACHINE\TEMP\Network\O { 
                    # REG DELETE $line.Line /f
                    # Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
                # }
                # HKEY_LOCAL_MACHINE\TEMP\Network\I { 
                    # REG DELETE $line.Line /f
                    # Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
                # }
                # HKEY_LOCAL_MACHINE\TEMP\Network\W { Write-Host "Deleted W" }
                # HKEY_LOCAL_MACHINE\TEMP\Network\X { Write-Host "Deleted X" }
            # }
			REG QUERY $line.Line /v "RemotePath"
        }
    }
                
    Write-Host Unloading... $folder.FullName -foregroundcolor "GREEN"
    REG UNLOAD HKLM\TEMP
}