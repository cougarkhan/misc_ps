$strPath = "C:\Temp\CSEC\profiles2"
$arrFolders = Get-ChildItem $strPath -recurse -force | where {$_.Extension -match "dat"}

Foreach ($folder in $arrFolders) {
    Write-Host `n`nLoading... $folder.FullName -foregroundcolor "GREEN"
    REG LOAD HKLM\TEMP $folder.FullName
    REG QUERY HKLM\TEMP\Network > c:\Temp\drives.txt
    $arrDrives = Select-String c:\Temp\drives.txt -pattern "HKEY_LOCAL_MACHINE\\TEMP\\Network\\"
    
    Foreach ($line in $arrDrives) {
        If ($line -ne $Null) {
            If ($line.Line -imatch "Network\\V") {
                REG DELETE $line.Line /f
                Write-Host "Deleted $line.Line from $folder.FullName" -foregroundcolor "RED"
            }
            If ($line.Line -imatch "Network\\Q") {
                REG QUERY $line.Line /v "RemotePath" > c:\Temp\remotepaths.txt
                $arrRemotePaths = Select-String c:\Temp\remotepaths.txt -pattern "RemotePath"
                Foreach ($path in $arrRemotePaths) {
                    $arrKey = $path.Line.Trim().Split()
                    $strData = $arrKey[2] -ireplace "businessop\\smcommon","bops-file1\SUPMANshare"
                    REG ADD $line.Line /v RemotePath /t REG_SZ /d $strData /f
                    REG QUERY $line.Line /v "RemotePath"
                    $arrKey = $Null
                }
            }
            If ($line.Line -imatch "Network\\F") {
                REG QUERY $line.Line /v "RemotePath" > c:\Temp\remotepaths.txt
                $arrRemotePaths = Select-String c:\Temp\remotepaths.txt -pattern "RemotePath"
                Foreach ($path in $arrRemotePaths) {
                    $arrKey = $path.Line.Trim().Split()
                    $strData = $arrKey[2] -ireplace "businessop\\smusers","bops-file1\SUPMANusers"
                    REG ADD $line.Line /v RemotePath /t REG_SZ /d $strData /f
                    REG QUERY $line.Line /v "RemotePath"
                    $arrKey = $Null
                }
            }
            If ($line.Line -imatch "Network\\X") {
                REG QUERY $line.Line /v "RemotePath" > c:\Temp\remotepaths.txt
                $arrRemotePaths = Select-String c:\Temp\remotepaths.txt -pattern "RemotePath"
                Foreach ($path in $arrRemotePaths) {
                    If ($path.Line -imatch "courier-invoices") {
                        $arrKey = $path.Line.Trim().Split()
                        $strData = $arrKey[2] -ireplace "businessop","bops-file1"
                        REG ADD $line.Line /v RemotePath /t REG_SZ /d $strData /f
                        REG QUERY $line.Line /v "RemotePath"
                        $arrKey = $Null
                    }
                }
            }
            If ($line.Line -imatch "Network\\T" -OR `
                $line.Line -imatch "Network\\M" -OR `
                $line.Line -imatch "Network\\W" -OR `
                $line.Line -imatch "Network\\G" -OR `
                $line.Line -imatch "Network\\J" -OR `
                $line.Line -imatch "Network\\K") {
                REG QUERY $line.Line /v "RemotePath" > c:\Temp\remotepaths.txt
                $arrRemotePaths = Select-String c:\Temp\remotepaths.txt -pattern "RemotePath"
                Foreach ($path in $arrRemotePaths) {
                    $arrKey = $path.Line.Trim().Split()
                    $strData = $arrKey[2] -ireplace "businessop","bops-file1"
                    REG ADD $line.Line /v RemotePath /t REG_SZ /d $strData /f
                    REG QUERY $line.Line /v "RemotePath"
                    $arrKey = $Null
                }
            }
        }
    }
        
    Write-Host Unloading... $folder.FullName -foregroundcolor "GREEN"
    REG UNLOAD HKLM\TEMP
}